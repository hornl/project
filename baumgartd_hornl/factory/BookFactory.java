/** 
 * Course: SE2811
 * Term: Winter 201_-201_
 * Assignment: Final Project
 * Author: Dan Baumgart, Lily Horn
 * Date: 2/13/2015
 */
package factory;

import composite.*;

/**
 * Represents a BookFactory object.
 * 
 * @author baumgartd, honrl
 *
 */
public class BookFactory extends MediaFactory {

	/**
	 * Creates a book with the specified attributes.
	 */
	@Override
	public Media createMedia(String title, String description, double price,
			double weight) {
		return new Book(title, description, price, weight);
	}

}
