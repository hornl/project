/** 
 * Course: SE2811
 * Term: Winter 201_-201_
 * Assignment: Final Project
 * Author: Dan Baumgart, Lily Horn
 * Date: 2/13/2015
 */
package factory;

import composite.*;

/**
 * Represents a DVDFactory object.
 * 
 * @author baumgartd, hornl
 *
 */
public class DVDFactory extends MediaFactory {

	/**
	 * Creates a DVD object with the specified attributes.
	 */
	@Override
	public Media createMedia(String title, String description, double price,
			double weight) {
		return new DVD(title, description, price, weight);
	}

}
