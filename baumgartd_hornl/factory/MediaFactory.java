/** 
 * Course: SE2811
 * Term: Winter 201_-201_
 * Assignment: Final Project
 * Author: Dan Baumgart, Lily Horn
 * Date: 2/13/2015
 */
package factory;

import composite.*;

/**
 * Represents a way to create different media types.
 * 
 * @author baumgartd,hornl
 *
 */
public abstract class MediaFactory {

	/**
	 * 
	 * @param title
	 *            - the media types title
	 * @param description
	 *            - A description of the media (publisher, director...)
	 * @param price
	 *            - the items price
	 * @param weight
	 *            - the weight of the object for shipping
	 * @return - newly created media
	 */
	public abstract Media createMedia(String title, String description,
			double price, double weight);

}
