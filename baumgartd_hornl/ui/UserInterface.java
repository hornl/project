/** 
 * Course: SE2811
 * Term: Winter 201_-201_
 * Assignment: Final Project
 * Author: Dan Baumgart, Lily Horn
 * Date: 2/13/2015
 */
package ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.*;

import composite.Media;
import main.OrderManager;

/**
 * 
 * @author baumgartd, hornl Represents the user interface for the user to
 *         interact with.
 *
 */
public class UserInterface extends JFrame {

	/**
	 * auto generated ID
	 */
	private static final long serialVersionUID = 1L;
	private OrderManager manager;

	// Button commands
	private String cmdViewBooks = "Books";
	private String cmdViewCDs = "CDs";
	private String cmdViewDVDs = "DVDs";
	private String cmdAddItem = "Add";

	// Inventory lists
	private List<Media> books;
	private List<Media> dvds;
	private List<Media> cds;

	// The JList to hold the selected inventory items
	private JList<Media> itemView;

	/**
	 * The constructor for the class.
	 * 
	 * @param books
	 *            - list of book's in inventory
	 * @param dvds
	 *            - list of dvd's in inventory
	 * @param cds
	 *            - list of cd's in inventory
	 */
	public UserInterface(List<Media> books, List<Media> dvds, List<Media> cds) {

		manager = OrderManager.getInstance();
		this.books = books;
		this.cds = cds;
		this.dvds = dvds;
		createGUI();
	}

	/**
	 * Create the user interface.
	 */
	private void createGUI() {
		// Setup the window
		EventHandler handler = new EventHandler();
		setTitle("Media store");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setResizable(false);
		setSize(590, 600);
		setLocation(10, 10);

		Container contentPane = getContentPane();
		BorderLayout layout = new BorderLayout();
		layout.setHgap(0);
		layout.setVgap(0);
		contentPane.setLayout(layout);

		// North

		// Banner image
		BufferedImage picture = null;
		Dimension imageDimension = new Dimension(495, 95);
		JPanel northPanel = new JPanel();
		JLabel pictureLabel = null;

		try {
			picture = ImageIO.read(new File("resources/image1.jpg"));
			pictureLabel = new JLabel(new ImageIcon(picture));
			pictureLabel.setSize(imageDimension);
			pictureLabel.setPreferredSize(imageDimension);
			pictureLabel.setMinimumSize(imageDimension);
			pictureLabel.setMaximumSize(imageDimension);
			northPanel.add(pictureLabel);
			contentPane.add(pictureLabel, BorderLayout.NORTH);
		} catch (IOException e) {
			northPanel.add(new JLabel("Couldn't load image"),
					BorderLayout.NORTH);
		}

		// West border
		Dimension buttonDimensions = new Dimension(102, 30);
		Dimension westpanelDimension = new Dimension(102, 505);

		JPanel westPanel = new JPanel();
		westPanel.setBackground(Color.WHITE);
		westPanel.setLayout(new BoxLayout(westPanel, BoxLayout.Y_AXIS));
		westPanel.setPreferredSize(westpanelDimension);
		westPanel.setSize(westpanelDimension);
		westPanel.setMinimumSize(westpanelDimension);
		westPanel.setMaximumSize(westpanelDimension);

		// "ViewBooks" button
		JButton viewBooks = new JButton("View Books");
		viewBooks.setPreferredSize(buttonDimensions);
		viewBooks.setMinimumSize(buttonDimensions);
		viewBooks.setMaximumSize(buttonDimensions);
		viewBooks.addActionListener(handler);
		viewBooks.setActionCommand(cmdViewBooks);
		westPanel.add(viewBooks);

		// "ViewDVDs" button
		JButton viewDVDs = new JButton("View DVDs");
		viewDVDs.setPreferredSize(buttonDimensions);
		viewDVDs.setMinimumSize(buttonDimensions);
		viewDVDs.setMaximumSize(buttonDimensions);
		viewDVDs.addActionListener(handler);
		viewDVDs.setActionCommand(cmdViewDVDs);
		westPanel.add(viewDVDs);

		// "ViewCDs" button
		JButton viewCDs = new JButton("View CDs");
		viewCDs.setPreferredSize(buttonDimensions);
		viewCDs.setMinimumSize(buttonDimensions);
		viewCDs.setMaximumSize(buttonDimensions);
		viewCDs.addActionListener(handler);
		viewCDs.setActionCommand(cmdViewCDs);
		westPanel.add(viewCDs);

		// "AddItem" button

		JButton addItem = new JButton("Add Item");
		addItem.setPreferredSize(buttonDimensions);
		addItem.setMinimumSize(buttonDimensions);
		addItem.setMaximumSize(buttonDimensions);
		addItem.addActionListener(handler);
		addItem.setActionCommand(cmdAddItem);
		westPanel.add(addItem);

		contentPane.add(westPanel, BorderLayout.WEST);

		// Center
		JPanel centerPanel = new JPanel(new BorderLayout());
		Dimension centerDimension = new Dimension(393, 498);
		centerPanel.setPreferredSize(centerDimension);
		centerPanel.setMinimumSize(centerDimension);
		centerPanel.setMaximumSize(centerDimension);

		// Create the JList to hold the selected inventory items
		itemView = new JList<Media>(new DefaultListModel<Media>());
		Font font = new Font("Arial", Font.PLAIN, 12);
		itemView.setFont(font);
		JScrollPane spPLContents = new JScrollPane(itemView,
				JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		spPLContents.setBounds(20, 40, 458, 340);
		centerPanel.add(spPLContents, BorderLayout.CENTER);

		JLabel lblContents = new JLabel();
		lblContents.setBounds(20, 20, 458, 20);
		lblContents.setText("Currently available:");
		centerPanel.add(lblContents, BorderLayout.NORTH);

		contentPane.add(centerPanel, BorderLayout.CENTER);

		// pack();
		setVisible(true);
	}

	/**
	 * Load the book's into the JList.
	 */
	private void loadBooks() {
		DefaultListModel<Media> list = (DefaultListModel<Media>) itemView
				.getModel();
		list.clear();

		for (Media m : books) {
			list.addElement(m);
		}
	}

	/**
	 * Load the dvd's into the JList.
	 */
	private void loadDVDs() {
		DefaultListModel<Media> list = (DefaultListModel<Media>) itemView
				.getModel();
		list.clear();

		for (Media m : dvds) {
			list.addElement(m);
		}
	}

	/**
	 * Load the cd's into the JList.
	 */
	private void loadCDs() {
		DefaultListModel<Media> list = (DefaultListModel<Media>) itemView
				.getModel();
		list.clear();

		for (Media m : cds) {
			list.addElement(m);
		}
	}

	/**
	 * Add an item to the shopping cart.
	 */
	private void addItem() {
		List<Media> itemsToAdd = itemView.getSelectedValuesList();
		for (Media m : itemsToAdd) {
			manager.addToCart(m);
		}
	}

	/**
	 * Decide what method is to be called.
	 * 
	 * @author baumgartd, hornl
	 *
	 */
	private class EventHandler implements ActionListener {

		/**
		 * Selects the type of media to be shown.
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			String cmd = e.getActionCommand();
			if (cmd.equals(cmdViewBooks))
				loadBooks();
			if (cmd.equals(cmdViewCDs))
				loadCDs();
			if (cmd.equals(cmdViewDVDs))
				loadDVDs();
			if (cmd.equals(cmdAddItem))
				addItem();
		}

	}

}
