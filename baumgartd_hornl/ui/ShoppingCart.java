/** 
 * Course: SE2811
 * Term: Winter 201_-201_
 * Assignment: Final Project
 * Author: Dan Baumgart, Lily Horn
 * Date: 2/13/2015
 */
package ui;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.swing.*;

import observer.Observer;
import composite.Media;
import main.*;

/**
 * 
 * @author baumgartd, hornl Represents a shopping cart object.
 *
 */
public class ShoppingCart extends JFrame implements Observer {

	/**
	 * Auto generated ID
	 */
	private static final long serialVersionUID = 1L;
	private JList<Media> itemList;
	private String cmdRemove = "Remove";
	private String cmdCheckOut = "CheckOut";
	private volatile static ShoppingCart cart;
	private OrderManager manager;
	private Collection<Media> orderedItems;

	private ShoppingCart() {

		EventHandler eventHandler = new EventHandler();
		manager = OrderManager.getInstance();
		orderedItems = new ArrayList<Media>();

		manager.addObserver(this);

		// Set up the window
		setTitle("Shopping Cart");
		setSize(350, 400);
		setLocation(200, 200);
		setResizable(true);
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		setVisible(true);

		Container contentPane = getContentPane();
		BorderLayout layout = new BorderLayout(0, 0);
		contentPane.setLayout(layout);

		// The panel to hold the buttons
		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout(FlowLayout.CENTER));
		panel.setBackground(new Color(113, 142, 169));

		// Remove button
		JButton removeBtn = new JButton(cmdRemove);
		removeBtn.addActionListener(eventHandler);
		panel.add(removeBtn);

		// Checkout button
		JButton checkoutBtn = new JButton(cmdCheckOut);
		checkoutBtn.addActionListener(eventHandler);
		panel.add(checkoutBtn);

		contentPane.add(panel, BorderLayout.SOUTH);

		// Create the JList that will hold the list of ordered items
		itemList = new JList<Media>(new DefaultListModel<Media>());
		Font font = new Font("Arial", Font.PLAIN, 12);
		itemList.setFont(font);
		JScrollPane spPLContents = new JScrollPane(itemList,
				JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		spPLContents.setBounds(20, 40, 458, 340);
		contentPane.add(spPLContents, BorderLayout.CENTER);

		JLabel lblContents = new JLabel();
		lblContents.setBounds(20, 20, 458, 20);
		lblContents.setText("Items in cart:");
		contentPane.add(lblContents, BorderLayout.NORTH);

		contentPane.validate();
		contentPane.repaint();
	}

	/**
	 * Only allow 1 shopping cart per "user"
	 * 
	 * @return - the only instance of shoppingCart
	 */
	public static ShoppingCart getInstance() {
		if (cart == null) {
			synchronized (ShoppingCart.class) {
				if (cart == null)
					cart = new ShoppingCart();
			}
		}

		return cart;
	}

	/**
	 * Checkout the ordered items.
	 */
	private void handleCheckOut() {
		manager.checkout(orderedItems);
	}

	/**
	 * Remove the selected items.
	 */
	private void handleRemove() {
		List<Media> listItems = itemList.getSelectedValuesList();
		for (Media m : listItems) {
			manager.removeItem(m);
		}
	}

	/**
	 * Handles any events for the ShoppingCart
	 * 
	 * @author baumgartd, hornl
	 * 
	 */
	private class EventHandler implements ActionListener {
		@Override
		/**
		 * Decide what method gets called depending the the button pushed.
		 */
		public void actionPerformed(ActionEvent e) {
			String cmd = e.getActionCommand();
			if (cmd.equals(cmdRemove))
				handleRemove();
			if (cmd.equals(cmdCheckOut))
				handleCheckOut();
		}
	}

	/**
	 * Update the current list of ordered items.
	 */
	@Override
	public void update(Collection<Media> updatedList) {
		DefaultListModel<Media> list = (DefaultListModel<Media>) itemList
				.getModel();
		list.clear();
		for (Media m : updatedList) {
			System.out.println(m);
			list.addElement(m);
		}
	}
}
