/** 
 * Course: SE2811
 * Term: Winter 201_-201_
 * Assignment: Final Project
 * Author: Dan Baumgart, Lily Horn
 * Date: 2/13/2015
 */
package composite;

import java.util.Collection;

/**
 * 
 * @author baumgartd, hornl
 * Is a "part" in the composite pattern. Represents a Movie Object.
 */
public class Movie implements Media{

	@Override
	public String getTitle() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public double getPrice() {
		// TODO Auto-generated method stub
		return 0.0;
	}

	@Override
	public double getWeight() {
		// TODO Auto-generated method stub
		return 0.0;
	}

	@Override
	public boolean addMedia(Media m) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean removeMedia(Media m) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Collection<Media> getMedia() {
		// TODO Auto-generated method stub
		return null;
	}

}
