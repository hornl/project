/** 
 * Course: SE2811
 * Term: Winter 201_-201_
 * Assignment: Final Project
 * Author: Dan Baumgart, Lily Horn
 * Date: 2/13/2015
 */
package composite;

import java.util.Collection;

/**
 * 
 * @author baumgartd, hornl Represents the component interface of the composite
 *         pattern. All components must implement each method.
 */
public interface Media {

	// Media type specific methods
	public String getTitle();

	public String getDescription();

	public double getPrice();

	public double getWeight();

	public String toString();

	// Composite specific methods
	public boolean addMedia(Media m);

	public boolean removeMedia(Media m);

	public Collection<Media> getMedia();
}
