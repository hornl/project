/** 
 * Course: SE2811
 * Term: Winter 201_-201_
 * Assignment: Final Project
 * Author: Dan Baumgart, Lily Horn
 * Date: 2/13/2015
 */
package composite;

import java.util.Collection;

/**
 * 
 * @author baumgartd, hornl Is a "part" in the composite pattern. Represents a
 *         Book object.
 */
public class Book implements Media {

	private String title;
	private String description;
	private double price;
	private double weight;

	/**
	 * The constructor for a Book.
	 * 
	 * @param title
	 * @param description
	 * @param price
	 * @param weight
	 */
	public Book(String title, String description, double price, double weight) {
		this.title = title;
		this.description = description;
		this.price = price;
		this.weight = weight;
	}

	/**
	 * @return - the Book's title.
	 */
	@Override
	public String getTitle() {
		return title;
	}

	/**
	 * @return - the Book's description.
	 */
	@Override
	public String getDescription() {
		return description;
	}

	/**
	 * @return - the Book's price.
	 */
	@Override
	public double getPrice() {
		return price;
	}

	/**
	 * @ return - the Book's weight.
	 */
	@Override
	public double getWeight() {
		return weight;
	}

	/**
	 * @return - The book's title and price.
	 */
	@Override
	public String toString() {
		return "Title: " + getTitle() + " Price: $" + getPrice();
	}

	// "Null" implementation of composite methods for Parts...
	@Override
	public boolean addMedia(Media m) {
		return false;
	}

	@Override
	public boolean removeMedia(Media m) {
		return false;
	}

	@Override
	public Collection<Media> getMedia() {
		return null;
	}

}
