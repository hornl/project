/** 
 * Course: SE2811
 * Term: Winter 201_-201_
 * Assignment: Final Project
 * Author: Dan Baumgart, Lily Horn
 * Date: 2/14/2015
 */
package observer;

import java.util.Collection;
import composite.Media;

/**
 * 
 * @author baumgartd, hornl Describes the class that each Observer must
 *         implement. It is left up to them to implement.
 */
public interface Observer {

	public void update(Collection<Media> list);

}
