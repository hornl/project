/** 
 * Course: SE2811
 * Term: Winter 201_-201_
 * Assignment: Final Project
 * Author: Dan Baumgart, Lily Horn
 * Date: 2/13/2015
 */
package observer;

/**
 * 
 * @author baumgartd, hornl Describes the methods that each subject must
 *         implement. It is up to them to implement.
 */
public interface Subject {
	public boolean addObserver(Observer o);

	public boolean removeOberver(Observer o);

	public void notifyObserver();
}
