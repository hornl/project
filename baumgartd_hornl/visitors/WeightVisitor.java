/** 
 * Course: SE2811
 * Term: Winter 201_-201_
 * Assignment: Final Project
 * Author: Dan Baumgart, Lily Horn
 * Date: 2/13/2015
 */
package visitors;

import composite.Media;

/**
 * 
 * @author baumgartd, hornl Visits each component in the "shopping cart" and
 *         returns the total weight of the objects.
 */
public class WeightVisitor extends AbstractVisitor {
	private double totalWeight;

	/**
	 * @param - the media object to visit
	 */
	@Override
	public void vist(Media media) {
		totalWeight += media.getWeight();

	}

	/**
	 * @return - the total weight of all objects visited.
	 */
	@Override
	public Object getResult() {
		return totalWeight;
	}

}
