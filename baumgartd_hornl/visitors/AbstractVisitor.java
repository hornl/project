/** 
 * Course: SE2811
 * Term: Winter 201_-201_
 * Assignment: Final Project
 * Author: Dan Baumgart, Lily Horn
 * Date: 2/13/2015
 */
package visitors;

import composite.*;

/**
 * 
 * @author baumgartd, hornl All visitors must extend the AbstractVisitor.
 */
public abstract class AbstractVisitor {
	public abstract void vist(Media media);

	public abstract Object getResult();

}
