/** 
 * Course: SE2811
 * Term: Winter 201_-201_
 * Assignment: Final Project
 * Author: Dan Baumgart, Lily Horn
 * Date: 2/13/2015
 */
package visitors;

import java.util.HashMap;
import java.util.Map;

import composite.Media;

/**
 * 
 * @author baumgartd, hornl Visits all components in the "shopping cart" and
 *         returns a list of descriptions.
 *
 */
public class DescriptionVisiter extends AbstractVisitor {
	private Map<String, String> descriptions = new HashMap<String, String>();

	/**
	 * @param - the media object to visit
	 */
	@Override
	public void vist(Media media) {
		descriptions.put(media.getTitle(), media.getDescription());

	}

	/**
	 * @return - a list of descriptions of all objects visited
	 */
	@Override
	public Object getResult() {
		return descriptions;
	}

}
