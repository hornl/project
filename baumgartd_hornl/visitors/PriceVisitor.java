/** 
 * Course: SE2811
 * Term: Winter 201_-201_
 * Assignment: Final Project
 * Author: Dan Baumgart, Lily Horn
 * Date: 2/13/2015
 */
package visitors;

import composite.Media;

/**
 * 
 * @author baumgartd, hornl Visits each component in a "shopping cart" and get's
 *         the total cost.
 */
public class PriceVisitor extends AbstractVisitor {

	private double totalPrice = 0;

	/**
	 * @param - The media object to visit
	 */
	@Override
	public void vist(Media media) {
		totalPrice += media.getPrice();

	}

	/**
	 * @return - the total price of all media objects visited
	 */
	@Override
	public Object getResult() {
		return totalPrice;
	}

}
