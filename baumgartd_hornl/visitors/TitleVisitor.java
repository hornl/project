/** 
 * Course: SE2811
 * Term: Winter 201_-201_
 * Assignment: Final Project
 * Author: Dan Baumgart, Lily Horn
 * Date: 2/13/2015
 */
package visitors;

import java.util.Collection;
import java.util.ArrayList;

import composite.Media;

/**
 * 
 * @author baumgartd, hornl Visits each component in the "shopping cart" to
 *         retrieve a list of titles.
 */
public class TitleVisitor extends AbstractVisitor {
	private Collection<String> titles = new ArrayList<String>();

	/**
	 * @param - the media object to visit
	 */
	@Override
	public void vist(Media media) {
		titles.add(media.getTitle());

	}

	/**
	 * @return - a list of all titles of the media objects visited
	 */
	@Override
	public Object getResult() {
		return titles;
	}

}
