/** 
 * Course: SE2811
 * Term: Winter 201_-201_
 * Assignment: Final Project
 * Author: Dan Baumgart, Lily Horn
 * Date: 2/13/2015
 */
package main;

import java.util.ArrayList;

import composite.Media;
import factory.*;
import ui.ShoppingCart;
import ui.UserInterface;

/**
 * 
 * @author baumgartd, hornl The "driver" for the class. Creates the inventory
 *         lists for interface to display. Creates an instance of shopping cart.
 *
 */
public class Main {

	/**
	 * Creates instances of all needed instances of classes and runs the program. 
	 * Also sets the inventory of the shop, for demonstration purposes. 
	 * @param args
	 */
	public static void main(String[] args) {
		// The inventory lists
		ArrayList<Media> books = new ArrayList<Media>();
		ArrayList<Media> dvds = new ArrayList<Media>();
		ArrayList<Media> cds = new ArrayList<Media>();

		// The factories for creating the inventory
		MediaFactory bookFactory = new BookFactory();
		MediaFactory dvdFactory = new DVDFactory();
		MediaFactory cdFactory = new CDFactory();

		// The inventory
		books.add(bookFactory.createMedia("Book 1", "Description", 5.00, 3.0));
		dvds.add(dvdFactory.createMedia("DVD 1", "Description", 12.00, .1));
		cds.add(cdFactory.createMedia("CD 1", "Description", 13.00, .11));
		books.add(bookFactory.createMedia("Lord of the Rings", "A wonderful book", 25.00, 4));
		dvds.add(dvdFactory.createMedia("Mulan", "A wonderful movie", 20.00, .1));
		books.add(bookFactory.createMedia("Mists of Avalon", "A feminist view on the Arthurian legends", 30.00, 6.00));
		cds.add(cdFactory.createMedia("A wonderful CD", "A good cd", 80.00, 69.00));
		books.add(bookFactory.createMedia("Pawn of Prophecy", "Wonderful beginning to a fanasy epic", 20, 3));
		books.add(bookFactory.createMedia("The Joy Luck Club", "A good book", 14, 2));
		books.add(bookFactory.createMedia("The Bakers Boy", "Good Book", 5, 2));
		books.add(bookFactory.createMedia("The Sign of the Four", "Second Sherlock Holmes short story", 5, 2));
		cds.add(cdFactory.createMedia("A trial in Jazz", "Jazz CD", 1.00, 0.13));
		cds.add(cdFactory.createMedia("Rock the world", "A rock cd", 20.00, 0.22));
		dvds.add(dvdFactory.createMedia("The Lion King", "Good movie", 25.00, 1));
		dvds.add(dvdFactory.createMedia("Another DVD", "A horroble piece of work", 20, 1));
		
		
		new UserInterface(books, dvds, cds);
		ShoppingCart.getInstance();
	}
}
