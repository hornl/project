/** 
 * Course: SE2811
 * Term: Winter 201_-201_
 * Assignment: Final Project
 * Author: Dan Baumgart, Lily Horn
 * Date: 2/13/2015
 */
package main;

import observer.Observer;
import observer.Subject;
import composite.*;
import visitors.*;

import java.util.ArrayList;
import java.util.Collection;
import javax.swing.JOptionPane;

/**
 * 
 * @author baumgartd, hornl Manages the shopping cart and the order.
 */
public class OrderManager implements Subject {

	private static volatile OrderManager manager; // Only one per customer

	ArrayList<Media> medias = new ArrayList<Media>();
	ArrayList<Observer> observers = new ArrayList<Observer>();

	/**
	 * There should only be one instance of OrderManager per customer visit.
	 * 
	 * @return - the one instance of manager
	 */
	public static OrderManager getInstance() {
		if (manager == null) {
			synchronized (OrderManager.class) {
				if (manager == null)
					manager = new OrderManager();
			}
		}
		return manager;
	}

	/**
	 * Sends a visitor to each item in the list to retrieve the title of the
	 * object, the price, and the weight for shipping.
	 * 
	 * @param - a list of media items in the shopping cart
	 */
	public void checkout(Collection<Media> media) {
		TitleVisitor v = new TitleVisitor();
		PriceVisitor p = new PriceVisitor();
		WeightVisitor w = new WeightVisitor();
		for (Media m : medias) {
			v.vist(m);
			p.vist(m);
			w.vist(m);
		}

		/**
		 * Display the items bought, total weight, and the total price.
		 */
		JOptionPane.showMessageDialog(null,
				"You have ordered the items: " + v.getResult()
						+ "\nThe total weight is " + w.getResult() + " lbs");
		int choice = JOptionPane.showConfirmDialog(null, "The total price is $"
				+ p.getResult().toString() + "\nPlease Confirm the Purchase.");
		if (choice == JOptionPane.CANCEL_OPTION
				|| choice == JOptionPane.CANCEL_OPTION) {
			// Then do nothing
		} else if (choice == JOptionPane.YES_OPTION) {
			medias.clear();
			JOptionPane
					.showMessageDialog(null, "Thank you for your purchase! ");
		} else if (choice == JOptionPane.NO_OPTION) {
			JOptionPane.showMessageDialog(null,
					"Oh, I really hoped you would buy something...");
		}
		notifyObserver();
	}

	/**
	 * 
	 * @param - the meida item to remove from the shopping cart.
	 */
	public void removeItem(Media media) {
		if (medias.contains(media)) {
			medias.remove(media);
			notifyObserver();
		}
	}

	/**
	 * @param - the observer subscribing to this subject for updates.
	 */
	@Override
	public boolean addObserver(Observer o) {
		if (!observers.contains(o)) {
			observers.add(o);
			return true;
		}
		return false;
	}

	/**
	 * @param - the observer whom no longer wants to recieve updates.
	 */
	@Override
	public boolean removeOberver(Observer o) {
		if (observers.contains(o)) {
			observers.remove(o);
			return true;
		}
		return false;
	}

	/**
	 * Update all subscribed observers that something has changed.
	 */
	@Override
	public void notifyObserver() {
		for (Observer o : observers) {
			o.update(medias);
		}

	}

	/**
	 * 
	 * @param - the item to be added to the shopping cart.
	 */
	public void addToCart(Media m) {
		medias.add(m);
		notifyObserver();
	}

}
